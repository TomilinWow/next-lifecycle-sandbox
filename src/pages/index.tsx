import Head from 'next/head'
import { PageLayout } from '@/components/layout'
import styles from './home.module.css'
import "@uiw/react-textarea-code-editor/dist.css";
import { useCallback, useState } from "react";
import { AppCodeEditor } from "@/components/AppCodeEditor/AppCodeEditor";
import { ControlPanel } from "@/components/ControlPanel/ControlPanel";
import { transformJSX } from "@/utils/transformJSX";
import { strCounter, strUserList } from "@/data/data";
import { TypeExamples } from "@/models/models";
import { JsxBlock } from "@/components/JsxBlock/JsxBlock";
import { TraceBlock } from "@/components/TraceBlock/TraceBlock";

export default function Home() {
  const [code, setCode] = useState(
    ``
  );
  const [jsxBlocks, setJsxBlocks] = useState<string[]>([])

  const runCode = useCallback(() => {
    if (code) {
      try {
        setJsxBlocks(transformJSX(code))
      } catch (e) {
        alert('Error')
      }
    }
  }, [code])

  const onHandleChangeSelect = useCallback((value: TypeExamples) => {
    if (TypeExamples.COUNTER === value) {
      setCode(strCounter)
    } else {
      setCode(strUserList)
    }
  }, [])

  const handleChangeCode = useCallback((value: string) => {
    setCode(value)
  }, [])

  return (
    <>
      <Head>
        <title>Next sandbox lifecycle</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <PageLayout>
        <div className={styles.wrapper}>
          <div id='home-element' className={`${styles.column} ${styles.jsxColumn}`}>
           <ControlPanel runCode={runCode} setSelectValue={onHandleChangeSelect}/>
           <AppCodeEditor code={code} handleChangeCode={handleChangeCode} />
          </div>
          <div id='new' className={styles.column}>
            <JsxBlock jsxBlocks={jsxBlocks}/>
          </div>
          <div id='home-element' className={styles.column}>
            <TraceBlock jsxBlocks={jsxBlocks}/>
          </div>
        </div>
      </PageLayout>
    </>
  )
}
