import React from 'react';
import styles from "@/components/AppCodeEditor/app-code-editor.module.css";
import dynamic from "next/dynamic";

const CodeEditor = dynamic(
  () => import("@uiw/react-textarea-code-editor").then((mod) => mod.default),
  { ssr: false }
);

interface IAppCodeEditor {
  code: string;
  handleChangeCode: (value: string) => void;
}
export const AppCodeEditor: React.FC<IAppCodeEditor> = ({
  code,
  handleChangeCode,
}) => {

  return (
    <div className={styles.container}>
      <CodeEditor
      className={styles.codeEditor}
      value={code}
      data-color-mode="dark"
      language="jsx"
      placeholder="Please enter JSX code."
      onChange={(e) => handleChangeCode(e.target.value)}
      padding={15}
      style={{
        fontSize: 16,
        backgroundColor: "#f6eac1",
        fontFamily: 'ui-monospace,SFMono-Regular,SF Mono,Consolas,Liberation Mono,Menlo,monospace',
      }}
    />
    </div>

  )
};
