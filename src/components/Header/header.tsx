import styles from "@/pages/home.module.css";


export const Header = () => {
  return (
    <div className={styles.header}>
    <h1 className={styles.headerText}>
      SANDBOX-LIFECYCLE
    </h1>
  </div>
  )
}
