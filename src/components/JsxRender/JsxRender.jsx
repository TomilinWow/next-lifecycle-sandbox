import StringToReactComponent from "string-to-react-component";
import React, {Fragment} from "react";
import {useEffect} from "react-hook-tracer";

export const JsxRender = (props) => {
  const [isScriptLoaded, setIsScriptLoaded] = React.useState(false);

  useEffect(() => {
    const script = document.createElement("script");
    script.src = "https://unpkg.com/@babel/standalone/babel.min.js";
    script.async = true;
    script.onload = () => setIsScriptLoaded(true);

    document.body.appendChild(script);
  }, []);

  return (
    <>
      {/* @ts-ignore */}
      {isScriptLoaded && <StringToReactComponent>
        {props.content}
      </StringToReactComponent>}
    </>
  );
};
